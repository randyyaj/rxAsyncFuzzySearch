# RxAsyncFuzzySearch
 <br/>
Implementation of asynchronous fuzzy searching using apache.common.lang3 fuzzy search and rxjava 2.0.6 <br/>
This implementation can also be used for asynchronous query calls to Solr or Elastic Search. <br/>
Doing so will increase the query time since it'll process asynchronously vs synchronously. <br/>

- [RxJava](http://reactivex.io/)
- [Apache Commons Lang3](https://commons.apache.org/proper/commons-lang/)