package com.gitlab.randyyaj;

import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 * Copyright 2017 Randy Yang
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
 *
 * Implementation of asynchronous fuzzy searching using rxjava 2.0.6
 * @author randyyaj.gitlab.com
 * @version 3/4/17
 */
public class RxAsyncFuzzySearch {
    public static void main(String[] args) {
        List<String> dictionary = Arrays.asList("sol", "sor", "solra", "solr", "asolr", "asyn", "asynch", "async");
        List<String> wordList = Arrays.asList("sro", "slra", "solr", "solrx", "async");

        // Asynchronous Execution for fuzzy search
        Flowable.just(wordList)
            .flatMapIterable(v -> v)                    // return the iterable item
                .flatMap(item -> Flowable.just(item)    // create a Flowable object to run asynchronously
                    .subscribeOn(Schedulers.io())       // (optional) subscribe to a scheduler for async *important must have scheduler -- computation for computation work and io for io operations (db queries and rest calls)
                    .map(w -> w)                        // (optional) transforms happen here (mutates the object)
            ).blockingSubscribe(wordToMatchOn -> {      // register a Consumer (@see Consumer) to process the object returned above
                int highestScore = 0;
                int currentScore;
                String bestMatch = "";

                for (String word : dictionary) {
                    currentScore = StringUtils.getFuzzyDistance(wordToMatchOn, word, Locale.ENGLISH);

                    if (wordToMatchOn.equals(word)) {
                        currentScore = 100;
                    }

                    if (currentScore > highestScore) {
                        highestScore = currentScore;
                        bestMatch = word;
                    }
                }
                System.out.println("Candidate: " + wordToMatchOn + " - Best: " + bestMatch + " - Score: " + highestScore + "%");
            });
    }
}
